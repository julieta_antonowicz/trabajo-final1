using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agacharse : MonoBehaviour
{
    public LayerMask capaPiso;
    private bool enElSuelo = true;
    public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        { 
            transform.localScale = new Vector3(5.0f, 5.0f, 5.0f); 
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        enElSuelo = true;
    }
}