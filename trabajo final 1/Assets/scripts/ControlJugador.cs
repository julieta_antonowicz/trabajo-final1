using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour 
{ 
    public float rapidezDesplazamiento = 10.0f;
    public int vida = 100;
    public TMPro.TMP_Text textoContadorVida;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoGanar;
    public TMPro.TMP_Text textoColeccionable;
    private bool Ganar = false;
    public AudioSource AudioSource;
    private int Coleccionable1 = 0 ;

    void Start() 
    {
        GestorDeAudio.instancia.ReproducirSonido("musica");
        Cursor.lockState = CursorLockMode.Locked;
        AudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        setearTextos();

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void OnCollisionEnter(Collision collision)
    { 
        if (collision.gameObject.CompareTag("Bot"))
        {
            vida -= 20;
        }

        if (collision.gameObject.CompareTag("FinDeMapa"))
        {
            vida -= 100;
        }

        if(collision.gameObject.CompareTag("pisoGanar"))
        {
            Ganar = true;
            AudioSource.Play();
        }

        if (collision.gameObject.CompareTag("Coleccionable"))
        {
            Coleccionable1 += 1;
        }
    }

    public void setearTextos()
    {
        textoContadorVida.text = "Vida restante: " + vida.ToString();

        if(vida<=0)
        {
            textoGameOver.text = "PERDISTE";
        }

        if(Ganar)
        {
            SceneManager.LoadScene(1); 
        }

        textoColeccionable.text = "Items recolectados: " + Coleccionable1;
    }
}