using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doblesalto : MonoBehaviour
{
    public LayerMask capaPiso;
    public float magnitudSalto;
    public float saltovel;
    private bool enElSuelo = true;
    public int maxSalto = 2;
    public int saltoActual = 0;
    public Rigidbody rb;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSalto > saltoActual))
        {
            rb.velocity = new Vector3(0f, saltovel, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * saltovel, ForceMode.Impulse);
            enElSuelo = false;
            saltoActual++;
            GestorDeAudio.instancia.ReproducirSonido("salto");

            transform.localScale = new Vector3(10.67043f, 13.717f, 10.72014f);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        enElSuelo = true;
        saltoActual = 0;
    }
}